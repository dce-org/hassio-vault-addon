#!/bin/sh

mkdir -p /data/vault-storage /data/vault-backup
chown -R vault /data/vault-storage /data/vault-backup

docker-entrypoint.sh $@
