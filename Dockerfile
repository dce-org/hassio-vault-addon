FROM vault

ENV SKIP_SETCAP=true
ENV VAULT_LOCAL_CONFIG='{"ui":true, "backend": {"file": {"path": "/data/vault-storage"}}, "default_lease_ttl": "168h", "max_lease_ttl": "720h", "disable_mlock": true, "listener":{ "tcp" :{ "address": "0.0.0.0:8200", "tls_disable": 1}}}'

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
RUN chmod a+x /usr/local/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
CMD ["server"]